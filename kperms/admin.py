from urllib.parse import urlencode
from django.contrib import admin
from django.http import HttpResponseRedirect
from django.urls import reverse

from kommons.utils.model import get_str_from_model
from django_object_actions import DjangoObjectActions


class KPermsAdmin(DjangoObjectActions, admin.ModelAdmin):
    action_name = 'set_perms'

    def __init__(self, *args, **kwargs):
        if self.action_name not in self.changelist_actions:
            xs = self.changelist_actions
            xs = list(xs)
            xs.append(self.action_name)
            self.changelist_actions = tuple(xs)
        super().__init__(*args, **kwargs)

    def changelist_view(self, request, extra_context=None):
        resp = super().changelist_view(request, extra_context=extra_context)

        if self.action_name in self.changelist_actions:
            app_label = self.model._meta.app_label
            model_name = self.model._meta.model_name
            can_set_perms = request.user.has_perm(f'{app_label}.setperm_{model_name}')

            if not can_set_perms:
                obj_actions = extra_context['objectactions']
                rem_pos = None
                for pos, x in enumerate(obj_actions):
                    if x['name'] == self.action_name:
                        rem_pos = pos

                if rem_pos is not None:
                    obj_actions.pop(rem_pos)
                    extra_context['objectactions'] = obj_actions

        return resp

    def set_perms(self, request, queryset):
        app_model = get_str_from_model(self.model)
        extra_params = self.get_perm_params(request, queryset)
        params = urlencode(extra_params)
        url = reverse('kperms:model_perms', args=(app_model,))
        return HttpResponseRedirect(f'{url}?{params}')
    set_perms.label = '<i class="fa fa-user-lock"></i>&nbsp; Permissions'
    set_perms.attrs = {'class': 'modal-remote'}
    set_perms.short_description = 'Permissions'

    def get_perm_params(self, request, queryset):
        return {}

