from django.urls import path

from kperms import views


app_name = 'kperms'

urlpatterns = [
    path('model/<str:app_model>/', views.ModelPermsView.as_view(), name="model_perms"),
]
