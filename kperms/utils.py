from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType

from kommons.utils.model import get_model_from_str


def get_or_create_perm(perm_code):
    app, others = perm_code.split('.')
    action, model = others.split('_')

    model_class = get_model_from_str('%s.%s' % (app, model))
    verbose = model_class._meta.verbose_name

    codename = '%s_%s' % (action, model)
    name = 'Can %s %s' % (action, verbose.title())

    # Permission.objects.filter(p='0')

    ct = ContentType.objects.get_for_model(model_class)
    return Permission.objects.update_or_create(
        content_type=ct,
        codename=codename,
        defaults={'name': name}
    )[0]
