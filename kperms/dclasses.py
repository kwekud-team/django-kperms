from dataclasses import dataclass, field
from typing import List


@dataclass
class Response:
    is_valid: bool
    resp_code: str
    resp_msg: str

    def to_dict(self):
        return {
            'is_valid': self.is_valid,
            'resp_code': self.resp_code,
            'resp_msg': self.resp_msg
        }


class Attachments:
    filename: str
    content: str
    content_type: str


@dataclass
class MessageData:
    sender: str
    subject: str
    message: str
    recipients: List[str]
    cc: List[str] = field(default_factory=list)
    bcc: List[str] = field(default_factory=list)
    attachments: List[Attachments] = field(default_factory=list)


@dataclass
class TemplateData:
    code: str
    state: str
    template_path: str = ''
    extra_context: dict = field(default=dict)
