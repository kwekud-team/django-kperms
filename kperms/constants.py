from enum import Enum


class KmsgK(Enum):
    GATEWAY_EMAIL = 'email'
    GATEWAY_SMS = 'sms'

    BCK_MAIL_CONSOLE = 'kmsg.backends.smtp.django.DjConsoleBackend'
    BCK_MAIL_DJANGO_SMTP = 'kmsg.backends.smtp.django.DjEmailBackend'
    BCK_SMS_HUBTEL = 'kmsg.backends.sms.hubtel.HubtelSMSBackend'

    @classmethod
    def get_gateway(cls, backend):
        GATEWAY_MAP = {
            cls.GATEWAY_EMAIL.value: [cls.BCK_MAIL_CONSOLE.value, cls.BCK_MAIL_DJANGO_SMTP.value],
            cls.GATEWAY_SMS.value: [cls.BCK_SMS_HUBTEL.value]
        }

        gateway = ''
        for k,v in GATEWAY_MAP.items():
            if backend in v:
                gateway = k
                break

        return gateway
