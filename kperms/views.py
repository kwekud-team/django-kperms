# -*- coding: utf-8 -*-
from urllib import parse
from django.contrib import admin
from django.urls import reverse
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import Group

from kommons.utils.generic import boolify
from kommons.utils.model import get_model_from_str
from kommons.utils.http import get_request_site


class ModelPermsView(TemplateView):
    template_name = 'kperms/quick_perms.html'
    perm_types = ['add', 'change', 'delete', 'view', 'setperm']
    site = None
    app_model = None
    model_class = None

    def dispatch(self, request, *args, **kwargs):
        self.site = get_request_site(self.request)
        self.app_model = self.kwargs['app_model']
        self.model_class = get_model_from_str(self.app_model)
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        for permission in self.get_possible_perms():

            # Clear only perms for group workspace
            existing_grps = permission.group_set.all()
            qs = permission.group_set.through.objects.filter(permission=permission, group__in=existing_grps)
            qs.delete()

            # reassign all perms set in form
            id_list = request.POST.getlist(str(permission.pk))
            grps = Group.objects.filter(pk__in=id_list)
            permission.group_set.add(*grps)

        next_url = request.GET.get('next', '')
        if next_url and next_url != 'None':
            return HttpResponseRedirect(next_url)

        return super().get(request, *args, **kwargs)

    def get_possible_perms(self):
        skip_model_perms = self.request.GET.get('smp', False)

        query = Q()

        if not boolify(skip_model_perms):
            content_type = ContentType.objects.get_for_model(self.model_class, for_concrete_model=False)
            query = Q(content_type=content_type)

        model_classes = self.get_extra_models()
        for mc in model_classes:
            ct = ContentType.objects.get_for_model(mc, for_concrete_model=False)
            query.add(Q(content_type=ct), Q.OR)

        extra_perm_codes = self.get_extra_perm_codes()
        for pc in extra_perm_codes:
            ct = ContentType.objects.get_for_model(pc['model_class'], for_concrete_model=False)
            query.add(Q(content_type=ct, codename=pc['perm_code']), Q.OR)

        return Permission.objects.filter(query)

    def get_related_admin_models(self):
        inline_models = []
        admin_class = admin.site._registry.get(self.model_class, None)
        if admin_class:
            inline_models = [x.model for x in admin_class.inlines]

        return inline_models

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context.update({
            'perm_objects': self.get_obj_perms(),
            'extra_params': self.get_extra_params(),
            'page_title': self.model_class._meta.verbose_name,
            'func_url': reverse("kperms:model_perms", args=(self.app_model,))
        })
        return context

    def get_extra_models(self):
        app_models = self.request.GET.getlist('app_model')
        models = filter(lambda it: it is not None, [get_model_from_str(x) for x in app_models])
        return list(models)

    def get_extra_perm_codes(self):
        perm_codes = self.request.GET.getlist('perm_code')
        xs = []
        for x in perm_codes:
            model_class = None
            if '.' in x:
                s1 = x.split('.')
                app_label, perm_code = s1[0], '.'.join(s1[1:])
                if '_' in perm_code:
                    if '|' in perm_code:
                        s0 = perm_code.split('|')
                        perm_model, unique_proxy = s0[0], '.'.join(s0[1:])
                    else:
                        perm_model = perm_code

                    perm, model_name = perm_model.split('_')
                    model_class = get_model_from_str(f'{app_label}.{model_name}')

                if model_class:
                    xs.append({
                        'model_class': model_class,
                        'perm_code': perm_code
                    })

        return xs

    def get_extra_params(self):
        params = dict(self.request.GET.items())
        params['next'] = self.request.GET.get('next', self.request.META.get('HTTP_REFERER', ''))

        return parse.urlencode(params)

    def get_obj_perms(self):
        all_groups = Group.objects.all()
        perm_qs = self.get_possible_perms()

        xs = []
        for perm in perm_qs:
            sel_groups = []
            available_groups = perm.group_set.all()
            for grp in all_groups:
                if grp in available_groups:
                    sel_groups.append(grp)

            xs.append({
                'permission': perm,
                'all_groups': all_groups,
                'selected_groups': sel_groups,
            })

        return xs
